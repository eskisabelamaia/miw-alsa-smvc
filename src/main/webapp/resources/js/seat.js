$(function () {
    var settings = {
        rows: 5,
        cols: 10,
        rowCssPrefix: 'row-',
        colCssPrefix: 'col-',
        seatWidth: 35,
        seatHeight: 35,
        seatCss: 'seat',
        selectedSeatCss: 'selectedSeat',
	   selectingSeatCss: 'selectingSeat'
    };

    var init = function (reservedSeat) {
        var str = [], seatNo, className;
        for (i = 0; i < settings.rows; i++) {
            if (i != 2) {
    	       for (j = 0; j < settings.cols; j++) {
    	           seatNo = (i + j * settings.rows + 1);
    	           if (seatNo!=21 && seatNo!=22){
    	                className = settings.seatCss + ' ' + settings.rowCssPrefix + i.toString() + ' ' + settings.colCssPrefix + j.toString();
    	                if ($.isArray(reservedSeat) && $.inArray(seatNo, reservedSeat) != -1) {
                            className += ' ' + settings.selectedSeatCss;
    	                }
    	                str.push('<li class="' + className + '"' +
    	                'style="top:' + (i * settings.seatHeight).toString() + 'px;left:' + (j * settings.seatWidth).toString() + 'px">' +
    	                '<a title="' + seatNo + '">' + seatNo + '</a>' +
    	                '</li>');
    	            }
    	        }
            }
        }
        $('#place').html(str.join(''));
    };

    var bookedSeats = [5,10,25];
    init(bookedSeats);

    $('.' + settings.seatCss).click(function () {
    	if ($(this).hasClass(settings.selectedSeatCss)){
    	    alert('This seat is already reserved');
    	}
    	else{
    		$(this).toggleClass(settings.selectingSeatCss);
    	}
    });
    	
    $('#btnContinue').click(function () {
    	var str = [];
    	$.each($('#place li.'+ settings.selectingSeatCss + ' a'), function (index, value) {
    	    str.push($(this).attr('title'));
    	    str.join(',')
    	});
    	document.getElementById("seatsvalue").setAttribute('value', str);
    });
});