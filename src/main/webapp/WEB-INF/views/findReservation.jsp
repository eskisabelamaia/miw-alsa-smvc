<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html lang="en">
<jsp:include page="header.jsp"/>
<!-- NAVBAR
================================================== -->
  <body>
  	<jsp:include page="menu.jsp"/>

	<div class="container"> 
		<form:form commandName="reservation" cssClass="form-horizonal">
			<fieldset>
				<div class='form-row'>
		        	<form:errors path="reservationCode" cssClass="text-danger"/>
		        </div>
				<div class='form-row'>
		        	<div class="control-group">
		        		<label class="control-label"><spring:message code="reservationcode"/></label>
				  		<div class="controls">
				  			<form:input path="reservationCode" cssClass="form-control" required="required"/>
				  		</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="find"></label>
				  		<div class="controls">
							<button id="find" name="find" class="btn btn-success"><spring:message code="btnsearch"/></button>
				  		</div>
					</div>
		        </div>
	 		</fieldset>
		</form:form>
      	<hr class="featurette-divider">

      	<jsp:include page="footer.jsp"/>

    </div>
  </body>
</html>