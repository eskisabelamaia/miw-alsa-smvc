<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<jsp:include page="header.jsp"/>
<!-- NAVBAR
================================================== -->
  <body>
	<c:import url="menu.jsp"></c:import>
    <div class="container marketing">
      <div class="row">
        <div class="col-md-12">
          <form:form commandName="reservation">
            <fieldset>
		      <legend><spring:message code="reservationinfo"/></legend>
		      <c:choose>
			    <c:when test="${not empty reservationinf}">
			      <div class="table-responsive">
		            <table id="mytable" class="table table-bordred table-striped sortable">
		              <c:if test="${reservationinf.traveltype == 'roundtrip' }">
		                <thead>
		                  <th><spring:message code="reservationcode"/></th>
		                  <th><spring:message code="departure"/></th>
		                  <th><spring:message code="departuredate"/></th>
		                  <th><spring:message code="destination"/></th>
		                  <th><spring:message code="returndate"/></th>
		                  <th><spring:message code="seats"/></th>
		                  <th><spring:message code="price"/></th>
		                </thead>
		                <tbody>
		                  <tr>
		                    <td><c:out value="${reservationinf.reservationCode}"></c:out></td>
		                    <td><c:out value="${reservationinf.departureCity}"></c:out></td>
		                    <td><c:out value="${reservationinf.departureDate}"></c:out></td>
		                    <td><c:out value="${reservationinf.destinationCity}"></c:out></td>
		                    <td><c:out value="${reservationinf.returnDate}"></c:out></td>
		                    <td><c:out value="${reservationinf.traveltype}"></c:out></td>
		                    <td><c:out value="${reservationinf.seats}"></c:out></td>
		                    <td><c:out value="${reservationinf.price}"></c:out></td>
		                  </tr>
		                </tbody>
		              </c:if>
		              <c:if test="${reservationinf.traveltype == 'oneway' || reservationinf.traveltype == 'openreturn'}">
		                <thead>
		                  <th><spring:message code="reservationcode"/></th>
		                  <th><spring:message code="departure"/></th>
		                  <th><spring:message code="departuredate"/></th>
		                  <th><spring:message code="destination"/></th>
		                  <th><spring:message code="seats"/></th>
		                  <th><spring:message code="price"/></th>
		                </thead>
		                <tbody>
		                  <tr>
		                    <td><c:out value="${reservationinf.reservationCode}"></c:out></td>
		                    <td><c:out value="${reservationinf.departureCity}"></c:out></td>
		                    <td><c:out value="${reservationinf.departureDate}"></c:out></td>
		                    <td><c:out value="${reservationinf.destinationCity}"></c:out></td>
		                    <td><c:out value="${reservationinf.seats}"></c:out></td>
		                    <td><c:out value="${reservationinf.price}"></c:out></td>
		                  </tr>
		                </tbody>
		              </c:if>
		            </table>
		          </div>
		          <div class="row">
		            <div class="form-group">
                      <div class="col-sm-offset-2 col-sm-10">
                        <div class="pull-right">
                          <input type="button" class='btn btn-default' value="Print this page" onClick="window.print()">
                          <button class='btn btn-primary' type='submit'><spring:message code="cancel"/></button>
                        </div>
                      </div>
                    </div>
			      </div>
			    </c:when>
			    <c:otherwise>
				  <p><spring:message code="error.find"/></p>
			    </c:otherwise>
			  </c:choose>
		    </fieldset>
		  </form:form>
        </div>
      </div>

      <hr class="featurette-divider">

      <c:import url="footer.jsp"></c:import>

    </div>
  </body>
</html>