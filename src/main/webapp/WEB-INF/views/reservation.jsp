<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<jsp:include page="header.jsp"/>
<!-- NAVBAR
================================================== -->
<body>
    <c:import url="menu.jsp"></c:import>

 	<div class="container marketing">
      <form:form commandName="reservation" cssClass="form-horizontal">
        <fieldset>
          <legend><spring:message code="chooseseat"/></legend>
          <div class='form-row'>
		    <form:errors path="seats" cssClass="text-danger"/><br/>
		  </div>
          <div class="row">
            <div class="col-md-12">
              <div class="row">
	            <div class="col-md-12">
	                <form:input path="seats" id="seatsvalue" value=""/>
	                <div id="holder"> 
	                  <ul id="place">
	                  </ul>
	                </div>
	                <div class="seatDescription"> 
	                  <ul>
	                    <li style="background:url('resources/img/available_seat_img.gif') no-repeat;"><spring:message code="available"/></li>
	                    <li style="background:url('resources/img/booked_seat_img.gif') no-repeat;"><spring:message code="booked"/></li>
	                    <li style="background:url('resources/img/selected_seat_img.gif') no-repeat;"><spring:message code="selected"/></li>
	                  </ul>        
	                </div>
	            </div>
	          </div>
	          <div class='form-row'>
		    	<form:errors path="additionalOpt" cssClass="text-danger"/><br/>
		  	  </div>
	          <div class="row">
	            <div class="col-md-12">
	              <legend><spring:message code="additionalopt"/></legend>
	              <p><spring:message code="selectopt"/></p>
	              <div class="additionalopt">
	                <form:label path="additionalOpt" cssClass="radio inline">
					  <form:radiobutton path="additionalOpt" value="none" checked="checked" /> <spring:message code="none"/>
					</form:label>
					<form:label path="additionalOpt" cssClass="radio inline">
					  <form:radiobutton path="additionalOpt" value="bike"/> <spring:message code="bike"/>
					</form:label>
					<form:label path="additionalOpt" cssClass="radio inline">
					  <form:radiobutton path="additionalOpt" value="pet"/> <spring:message code="pet"/>
					</form:label>
	              </div>
	              <br>
	              <div class='form-row'>
		    		<form:errors path="hasInsurance" cssClass="text-danger"/><br/>
		  	  	  </div>
	              <p><spring:message code="insurance"/></p>
	              <div class="additionalopt">
	                <form:label path="hasInsurance" cssClass="radio inline">
					  <form:checkbox path="hasInsurance" value="none"/> <spring:message code="addinsurance"/>
					</form:label>
	              </div>
	            </div>
	          </div>
		      <div class="row">
		        <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    <div class="pull-right">
                      <button class='btn btn-primary' id="btnContinue" type='submit'><spring:message code="continue"/></button>
                    </div>
                  </div>
                </div>
			  </div>
            </div>
          </div>
      	  </fieldset>
      </form:form>
      <hr class="featurette-divider">

      <c:import url="footer.jsp"></c:import>

    </div>
    <script src="<c:url value="/resources/js/seat.js" />"></script>
  </body>
</html>