<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html lang="en">
<jsp:include page="header.jsp"/>
<!-- NAVBAR
================================================== -->
  <body>
  	<jsp:include page="menu.jsp"/>
	<div class="container"> 
		<form:form commandName="register" cssClass="form-horizonal">
			<fieldset>
		        <div class='form-row'>
		     		<div class="control-group">
		        		<form:label path="username" cssClass="control-label"><spring:message code="username"/></form:label>
				  		<div class="controls">
				  			<form:input path="username" cssClass="form-control" required="required"/>
				  		</div>
				  		<br/><form:errors path="username" cssClass="text-danger"/>
					</div>
					<div class="control-group">
		        		<form:label path="password" cssClass="control-label"><spring:message code="password"/></form:label>
				  		<div class="controls">
				  			<form:input path="password" type="password" cssClass="form-control" required="required"/>
				  		</div>
				  		<br/><form:errors path="password" cssClass="text-danger"/>
					</div>
					<div class="control-group">
						<label class="control-label" for="login"></label>
					  	<div class="controls">
							<button id="login" name="login" class="btn btn-success"><spring:message code="register"/></button>
					  	</div>
					</div>
		        </div>
	 		</fieldset>
		</form:form>
      	<hr class="featurette-divider">

      	<jsp:include page="footer.jsp"/>

    </div>
  </body>
</html>