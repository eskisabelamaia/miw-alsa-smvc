<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html lang="en">
<jsp:include page="header.jsp"/>
<!-- NAVBAR
================================================== -->
  <body>
  	<jsp:include page="menu.jsp"/>

    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img src="<c:url value="/resources/img/donostia.jpg"/>" alt="Donostia"/>
        </div>
        <div class="item">
          <img src="<c:url value="/resources/img/zumaia.jpg"/>" alt="Zumaia"/>
        </div>
        <div class="item">
          <img src="<c:url value="/resources/img/bilbo.jpg"/>" alt="Bilbo"/>
        </div>
      </div>
    </div>
    <div class="container marketing">

      <div class="row">
        <div class="col-lg-5">
        	<form:form commandName="reservation" cssClass="searchform">
        		<fieldset>
		        	<legend><spring:message code="search"/></legend>
		            <div class='form-row'>
		                <div class='col-xs-6 form-group required'>
		                	<form:label path="departureCity" cssClass="control-label"><spring:message code="departure"/></form:label>
		                	<form:select path="departureCity" required="required">
		                		<form:options items="${cities}"></form:options>
		                	</form:select>
		                </div>
		                <div class='col-xs-6 form-group required'>
		                	<form:label path="destinationCity" cssClass="control-label"><spring:message code="destination"/></form:label>
		                	<form:select path="destinationCity">
		                		<form:options items="${cities}"></form:options>
		                	</form:select>
		                </div>
		                <br><form:errors path="destinationCity" cssClass="text-danger"/>
		            </div>
		            <div class='form-row'>
		                <div class='col-xs-4 form-group required'>
					      <form:label path="traveltype" cssClass="radio inline">
					        <form:radiobutton path="traveltype" id="roundtrip" value="roundtrip"/> <spring:message code="roundtrip"/>
					      </form:label>
		                </div>
		                <div class='col-xs-4 form-group required'>
					      <form:label path="traveltype" cssClass="radio inline">
					        <form:radiobutton path="traveltype" id="oneway" value="oneway"/> <spring:message code="oneway"/>
					      </form:label>
		                </div>
		                <div class='col-xs-4 form-group required'>
					      <form:label path="traveltype" cssClass="radio inline">
					        <form:radiobutton path="traveltype" id="openreturn" value="openreturn"/><spring:message code="openreturn"/>
					      </form:label>
		                </div>
		                <br><form:errors path="traveltype" cssClass="text-danger"/>
		            </div>
		            <div class='form-row'>
		                <div class='col-xs-6 form-group required'>
		                  <form:label path="departureDate" cssClass="control-label"><spring:message code="departuredate"/></form:label>
		                  <div class="input-group date" id="timepicker">
		  					<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
		  					<form:input path="departureDate" cssClass="date-picker form-control" placeholder="${dateformat}" required="required"/>
		 				  </div>
		 				  <br><form:errors path="departureDate" cssClass="text-danger"/>
		                </div>
		                <div class='col-xs-6 form-group required'>
		                  <form:label path="returnDate" cssClass="control-label"><spring:message code="returndate"/></form:label>
		                  <div class="input-group date" id="timepicker">
		  					<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
		  					<form:input path="returnDate" name="returnDate" cssClass="date-picker form-control" placeholder="${dateformat}"/>
		 				  </div>
		 				  <br><form:errors path="returnDate" cssClass="text-danger"/>
		                </div>
		            </div>
		            <div class='form-row'>
		                <div class='col-md-12 form-group'>
					      <label class="control-label" for="seats"><spring:message code="qtyseats"/></label>
					      <div class="controls">
					        <form:select path="numberSeats" cssClass="input-large" required="required">
					        	<form:option value="1" label="1"/>
					        	<form:option value="2" label="2"/>
					        	<form:option value="3" label="3"/>
					        	<form:option value="4" label="4"/>
					        	<form:option value="5" label="5"/>
					        	<form:option value="6" label="6"/>
					        	<form:option value="7" label="7"/>
					        	<form:option value="8" label="8"/>
					        	<form:option value="9" label="9"/>
					        </form:select>
					  	  </div>
		                </div>
		            </div>
		            <div class='form-row'>
		                <div class='col-md-12 form-group'>
		                  <button class='form-control btn btn-primary' type='submit'><spring:message code="btnsearch"/></button>
		                </div>
		            </div>
		        </fieldset>
			</form:form>
        </div>
        <div class="col-lg-7">
          <div class="populardest">
            <legend><spring:message code="populardest"/></legend>
            <div class="row" style="margin-top:0px">
              <c:forEach items="${destinations}" var="city">
                <div class="col-md-4">
                  <div class="thumbnail">
                    <img src="<c:url value="/resources/img/${city}.jpg"/>" alt="Madrid"/>
                    <div class="caption">
                      <p><c:out value="${city}"></c:out></p>
                    </div>            
                  </div>
                </div>
              </c:forEach>
            </div>
          </div>
        </div>
      </div>

      <hr class="featurette-divider">
      <jsp:include page="footer.jsp"/>

    </div>
  </body>
</html>