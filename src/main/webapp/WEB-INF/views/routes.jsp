<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<jsp:include page="header.jsp"/>
<!-- NAVBAR
================================================== -->
  <body>
	<c:import url="menu.jsp"></c:import>
    <div class="container marketing">
      <div class="row">
        <div class="col-md-12">
          <form:form commandName="routes">
            <fieldset>
		      <legend><spring:message code="timetable"/></legend>
		          <div>
			        <input type="button" value="Print this page" onClick="window.print()">
			      </div>
		          <div class="table-responsive">
		            <table id="mytable" class="table table-bordred table-striped sortable">
		              <c:if test="${reservation.traveltype == 'roundtrip' }">
		                <thead>
		                  <th></th>
		                  <th><spring:message code="departure"/></th>
		                  <th><spring:message code="destination"/></th>
		                  <th><spring:message code="departuredate"/></th>
		                  <th><spring:message code="departuretime"/></th>
		                  <th><spring:message code="returntime"/></th>
		                  <th><spring:message code="roundtripprice"/></th>
		                </thead>
		                <tbody>
		              	  <c:forEach items="${routestable}" var="route">
		              	    <tr>
		                      <td><form:radiobutton path="id" name="routeid" value="${route.id}"/></td>
		                      <td><c:out value="${reservation.departureCity}"></c:out></td>
		                      <td><c:out value="${reservation.destinationCity}"></c:out></td>
		                      <td><c:out value="${reservation.departureDate}"></c:out></td>
		                      <td><c:out value="${route.departure}"></c:out></td>
		                      <td><c:out value="${route.arrival}"></c:out></td>
		                      <td><c:out value="${route.returnPrice}"></c:out></td>
		                    </tr>
		                    <tr>
		                  	  <td></td>
		                      <td><c:out value="${reservation.destinationCity}"></c:out></td>
		                      <td><c:out value="${reservation.departureCity}"></c:out></td>
		                      <td><c:out value="${reservation.returnDate}"></c:out></td>
		                      <td><c:out value="${route.destdeparture}"></c:out></td>
		                      <td><c:out value="${route.destarrival}"></c:out></td>
		                      <td></td>
		                      <td></td>
		                      <td></td>
		                    </tr>
		                  </c:forEach>
		                </tbody>
		              </c:if>
		              <c:if test="${reservation.traveltype == 'oneway' || reservation.traveltype == 'openreturn'}">
		                <thead>
		                  <th></th>
                          <th><spring:message code="departure"/></th>
		                  <th><spring:message code="destination"/></th>
		                  <th><spring:message code="departuredate"/></th>
		                  <th><spring:message code="departuretime"/></th>
		                  <th><spring:message code="returntime"/></th>
		                  <th><spring:message code="onewayprice"/></th>
		                </thead>
		                <tbody>
		              	  <c:forEach items="${routestable}" var="route">
		              	    <tr>
		                      <td><form:radiobutton path="id" name="routeid" value="${route.id}"/></td>
		                      <td><c:out value="${reservation.departureCity}"></c:out></td>
		                      <td><c:out value="${reservation.destinationCity}"></c:out></td>
		                      <td><c:out value="${reservation.departureDate}"></c:out></td>
		                      <td><c:out value="${route.departure}"></c:out></td>
		                      <td><c:out value="${route.arrival}"></c:out></td>
		                      <td><c:out value="${route.onewayPrice}"></c:out></td>
		                    </tr>
		              	  </c:forEach>
		                </tbody>
		              </c:if>
		            </table>
		          </div>
		          <div class="row">
		            <div class="form-group">
                      <div class="col-sm-offset-2 col-sm-10">
                        <div class="pull-right">
                          <a href="/alsa" class="btn btn-default"><spring:message code="back"/></a>
                          <button class='btn btn-primary' type='submit'><spring:message code="buy"/></button>
                        </div>
                      </div>
                    </div>
			      </div>
		        </fieldset>
			</form:form>
        </div>
      </div>

      <hr class="featurette-divider">

      <c:import url="footer.jsp"></c:import>

    </div>
  </body>
</html>