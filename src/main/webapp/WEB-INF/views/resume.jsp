<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<jsp:include page="header.jsp"/>
<!-- NAVBAR
================================================== -->
  <body>
	<c:import url="menu.jsp"></c:import>
    <div class="container marketing">
      <h1><spring:message code="reservationcode"/> <c:out value="${reservation.reservationCode}"></c:out></h1>
      <div class="row">
        <div class="col-md-12">
          <form:form commandName="routes">
            <fieldset>
		      <legend><spring:message code="user.information"/></legend>
		          <div>
		            <table id="mytable" class="table table-bordred table-striped sortable">
		              <thead>
		                <th><spring:message code="name"/></th>
		                <th><spring:message code="middlename"/></th>
		                <th><spring:message code="surname"/></th>
		                <th><spring:message code="numdoc"/></th>
		              </thead>
		              <tbody>
		              	  <tr>
		                    <td><c:out value="${user.firstName}"></c:out></td>
		                    <td><c:out value="${user.middleName}"></c:out></td>
		                    <td><c:out value="${user.surname}"></c:out></td>
		                    <td><c:out value="${user.identificationNumber}"></c:out></td>

		                  </tr>
		              </tbody>
		            </table>
		          </div>
		          <c:if test="${reservation.traveltype == 'roundtrip' }">      
	  	            <div>
		              <legend><spring:message code="routeinfo"/></legend>
		              <table id="mytable" class="table table-bordred table-striped sortable">
		                <thead>
		                  <th><spring:message code="departure"/></th>
		                  <th><spring:message code="destination"/></th>
		                  <th><spring:message code="departuredate"/></th>
		                  <th><spring:message code="departuretime"/></th>
		                  <th><spring:message code="returndate"/></th>
		                  <th><spring:message code="returntime"/></th>
		                  <th><spring:message code="seats"/></th>
		                  <th><spring:message code="price"/></th>
		                </thead>
		                <tbody>
		              	  <tr>
		                    <td><c:out value="${reservation.departureCity}"></c:out></td>
		                    <td><c:out value="${reservation.destinationCity}"></c:out></td>
		                    <td><c:out value="${reservation.departureDate}"></c:out></td>
		                    <td><c:out value="${route.departure}"></c:out></td>
		                    <td><c:out value="${reservation.returnDate}"></c:out></td>
		                    <td><c:out value="${route.arrival}"></c:out></td>
		                    <td><c:out value="${reservation.seats}"></c:out></td>
		                    <td><c:out value="${reservation.price}"></c:out></td>
		                  </tr>
		                </tbody>
		              </table>
		            </div>
		          </c:if>
		          <c:if test="${reservation.traveltype == 'oneway' || reservation.traveltype == 'openreturn'}">
		            <div>
		               <legend><spring:message code="routeinfo"/></legend>
		              <table id="mytable" class="table table-bordred table-striped sortable">
		                <thead>
                          <th><spring:message code="departure"/></th>
		                  <th><spring:message code="destination"/></th>
		                  <th><spring:message code="departuredate"/></th>
		                  <th><spring:message code="departuretime"/></th>
		                  <th><spring:message code="returntime"/></th>
		                  <th><spring:message code="seats"/></th>
		                  <th><spring:message code="onewayprice"/></th>
		                </thead>
		                <tbody>
		              	  <tr>
		                    <td><c:out value="${reservation.departureCity}"></c:out></td>
		                    <td><c:out value="${reservation.destinationCity}"></c:out></td>
		                    <td><c:out value="${reservation.departureDate}"></c:out></td>
		                    <td><c:out value="${route.departure}"></c:out></td>
		                    <td><c:out value="${route.arrival}"></c:out></td>
		                    <td><c:out value="${reservation.seats}"></c:out></td>
		                    <td><c:out value="${reservation.price}"></c:out></td>
		                  </tr>
		                </tbody>
		              </table>
		            </div> 
		          </c:if>
		          <div>
		            <legend><spring:message code="additionalinfo"/></legend>
		            <table id="mytable" class="table table-bordred table-striped sortable">
		              <thead>
		                <th> <p><spring:message code="additionalopt"/></p></th>
		                <th> <p><spring:message code="insurance"/></p></th>
		              </thead>
		              <tbody>
		              	  <tr>
		              	  	<c:if test="${reservation.additionalOpt == 'None' }">
		                    	<td> <p><spring:message code="noneinfo"/></p></td>
		                    </c:if>
		                    <c:if test="${reservation.additionalOpt == 'pet' }">
		                    	<td> <p><spring:message code="pet"/></p></td>
		                    </c:if>
		                    <c:if test="${reservation.additionalOpt == 'bike' }">
		                    	<td> <p><spring:message code="bike"/></p></td>
		                    </c:if>
		                    <c:if test="${reservation.hasInsurance = true}">
		                    	<td><p><spring:message code="insuranceinfo"/></p></td>
		                    </c:if>
		                   	<c:if test="${reservation.hasInsurance = false}">
		                    	<td><p><spring:message code="insuranceinfono"/></p></td>
		                    </c:if>
		                </tr>
		              </tbody>
		            </table>
		          </div>
		          <div class="row">
		            <div class="form-group">
                      <div class="col-sm-offset-2 col-sm-10">
                        <div class="pull-right">
                          <button class='btn btn-primary' type='submit'><spring:message code="buy"/></button>
                        </div>
                      </div>
                    </div>
			      </div>
		        </fieldset>
			</form:form>
        </div>
      </div>

      <hr class="featurette-divider">

      <c:import url="footer.jsp"></c:import>

    </div>
  </body>
</html>