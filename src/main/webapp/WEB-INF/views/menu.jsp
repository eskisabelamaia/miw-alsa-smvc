<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<div class="logo">
	<a href="/alsa">
		<img src="<c:url value="/resources/img/alsa.jpg"/>" alt="Alsa">
	</a>
	<div>
		<p><spring:message code="counter"/> <c:out value="${visitorCounter}"></c:out></p>
	</div>
</div>
<nav class="navbar navbar-inverse">
	<div class="container">
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li><a href="/alsa"><spring:message code="nav.home"/></a></li>
				<li><a  href="<c:url value='/findReservation' />"><spring:message code="nav.find"/></a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="?lang=es"><spring:message code="nav.spanish"/></a></li>
				<li><a href="?lang=en"><spring:message code="nav.english"/></a></li>
				<c:choose>
				    <c:when test="${empty user}"> 
				        <li><a href="<c:url value='/login' />"><spring:message code="login"/></a></li>
				    </c:when> 
				    <c:otherwise> 
				    	<li><a href="<c:url value='/profile' />"><c:out value="${usersname}"></c:out></a></li>
						<li><a href="<c:url value='/logout' />"><spring:message code="nav.logout"/></a></li>
				    </c:otherwise> 
				</c:choose>
			</ul>
		</div>
	</div>
</nav>