<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<jsp:include page="header.jsp"/>
<!-- NAVBAR
================================================== -->
  <body>
	<c:import url="menu.jsp"></c:import>
    <div class="container marketing">
      <div class="row">
        <div class="col-md-12">
          <form:form commandName="reservation">
            <fieldset>
		      <legend>Reservation record</legend>
		          <div class="table-responsive">
		            <table id="mytable" class="table table-bordred table-striped sortable">
		              <thead>
		                <th><spring:message code="reservationcode"/></th>
		                <th><spring:message code="departure"/></th>
		                <th><spring:message code="departuredate"/></th>
		                <th><spring:message code="returndate"/></th>
		                  <th><spring:message code="price"/></th>
		                <th><spring:message code="seats"/></th>
		                <th><spring:message code="price"/></th>
		              </thead>
		              <tbody>
		                <tr>
		                  <c:forEach items="${reservations}" var="reservation">
		              	  <tr>
		                    <td><c:out value="${reservation.reservationCode}"></c:out></td>
		                    <td><c:out value="${reservation.departureCity}"></c:out></td>
		                    <td><c:out value="${reservation.departureDate}"></c:out></td>
		                    <td><c:out value="${reservation.destinationCity}"></c:out></td>
		                    <c:if test="${reservation.traveltype == 'roundtrip' }">
		                    	<td><c:out value="${reservation.returnDate}"></c:out></td>
		                    </c:if>
		                    <c:if test="${reservation.traveltype == 'oneway' || reservation.traveltype == 'openreturn'}">
		                    	<td><c:out value="${reservation.traveltype}"></c:out></td>
		                    </c:if>
		                    <td><c:out value="${reservation.seats}"><spring:message code="euro"/></c:out></td>
		                    <td><c:out value="${reservation.price}"><spring:message code="euro"/></c:out></td>
		                </tr>
		              	</c:forEach>
		                </tr>
		              </tbody>
		            </table>
		          </div>
		        </fieldset>
			</form:form>
        </div>
      </div>

      <hr class="featurette-divider">

      <c:import url="footer.jsp"></c:import>

    </div>
  </body>
</html>