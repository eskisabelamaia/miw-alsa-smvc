<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<footer>
	<p class="pull-right"><a href="#"><spring:message code="backtop"/></a></p>
	<p><spring:message code="copyright"/></p>
</footer>