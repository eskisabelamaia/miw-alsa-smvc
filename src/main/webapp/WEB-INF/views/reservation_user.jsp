<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<jsp:include page="header.jsp"/>
<!-- NAVBAR
================================================== -->
  <body>
    <c:import url="menu.jsp"></c:import>

 	<div class="container marketing">
      <form:form commandName="user" cssClass="form-horizontal">
        <fieldset>
		  <legend><spring:message code="user.information"/></legend>
		  <div class='form-row'>
		    <form:errors path="firstName" cssClass="text-danger"/><br/>
		    <form:errors path="surname" cssClass="text-danger"/><br/>
		    <form:errors path="identificationNumber" cssClass="text-danger"/>
		  </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label class="col-sm-2 control-label"><spring:message code="name"/></label>
                <div class="col-sm-4">
                  <form:input path="firstName" cssClass="form-control" required="required"/>
                </div>
                <label class="col-sm-2 control-label"><spring:message code="middlename"/></label>
                <div class="col-sm-4">
                  <form:input path="middleName" cssClass="form-control"/>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label"><spring:message code="surname"/></label>
                <div class="col-sm-4">
                  <form:input path="surname" cssClass="form-control" required="required"/>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label"><spring:message code="iddoc"/></label>
                <div class="col-sm-4">
		          <form:select path="indetificationDocument" required="required">
		            <form:options items="${docType}"></form:options>
		          </form:select>
                </div>
                <label class="col-sm-2 control-label"><spring:message code="numdoc"/></label>
                <div class="col-sm-4">
                  <form:input path="identificationNumber" cssClass="form-control" required="required"/>
                </div>
              </div>
		      <div class="row">
		        <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    <div class="pull-right">
                      <a href="/alsa/routes" class="btn btn-default"><spring:message code="back"/></a>
                      <button class='btn btn-primary' type='submit'><spring:message code="continue"/></button>
                    </div>
                  </div>
                </div>
			  </div>
            </div>
          </div>
      	  </fieldset>
      </form:form>

      <hr class="featurette-divider">

      <c:import url="footer.jsp"></c:import>

    </div>
  </body>
</html>