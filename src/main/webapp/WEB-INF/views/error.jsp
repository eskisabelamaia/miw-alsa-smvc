<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<c:import url="head.jsp"></c:import>
<!-- NAVBAR
================================================== -->
  <body>
	<c:import url="menu.jsp"></c:import>
    <div class="container marketing">
      <div class="row">
        <div class=".col-md-12">
          <div class="hero-unit center">
            <h1 class="text-center">Sorry! </h1>
            <br/>
            <p class="text-center">We could not make the reservation!</p>
            <br/>
            <div class="pull-right">
              <a href="/alsa" class="btn btn-large btn-primary"><spring:message code="home"/></a>
              <a href="/alsa/profile" class="btn btn-large btn-primary"><spring:message code="account"/></a>
            </div>
          </div>
        </div>
      </div>

      <hr class="featurette-divider">

      <c:import url="footer.jsp"></c:import>

    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<c:url value="/resources/js/jquery.min.js" />"></script>
    <script src="<c:url value="/resources/js/bootstrap.min.js.js" />"></script>
  </body>
</html>