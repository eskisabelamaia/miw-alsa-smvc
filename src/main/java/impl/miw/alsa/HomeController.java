package impl.miw.alsa;

import impl.miw.alsa.business.cities.CitiesManager;
import impl.miw.alsa.business.route.RoutesManager;
import impl.miw.alsa.presentation.counter.VisitorCounter;

import java.util.Vector;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.miw.alsa.model.Reservations;

/**
 * Handles requests for the application home page.
 */

@Controller
@SessionAttributes("reservation")
public class HomeController {
		
	@Autowired
	private CitiesManager citiesManager;
	
	@Autowired
	private RoutesManager routesManager;
		
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public String homePost(Model model, @Valid @ModelAttribute("reservation") Reservations reservation, BindingResult result, HttpSession session) throws Exception {
		Home homeValidator = new Home();
		homeValidator.validate(reservation, result);
		if (!result.hasErrors()){
			reservation.setDepartureCity(reservation.getDepartureCity());
			reservation.setDestinationCity(reservation.getDestinationCity());
			reservation.setDepartureDate(reservation.getDepartureDate());
			reservation.setTraveltype(reservation.getTraveltype());
			reservation.setNumberSeats(reservation.getNumberSeats());
			
			if(reservation.getTraveltype().equals("roundtrip")) {
				reservation.setReturnDate(reservation.getReturnDate());
			} else if(reservation.getTraveltype().equals("oneway") || reservation.getTraveltype().equals("openreturn")) {
				reservation.setReturnDate("00-00-0000");
			}
			return "redirect:routes";
		} else {
			model.addAttribute("cities", getCityName());
			return "index";
		}
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String homeGet(Model model, @ModelAttribute("reservation") Reservations reservation, RedirectAttributes redirectAttributes, BindingResult result, HttpSession session) throws Exception {
		model.addAttribute("cities", getCityName());
		model.addAttribute("destinations", getMostPopularDestinations());
		model.addAttribute("visitorCounter",VisitorCounter.getInstance().inc());
		String url ="/";
		session.setAttribute("url", url);
		return "index";
	}
	
	@ModelAttribute("reservation")
	public Reservations getReservation(){
		return new Reservations();
	}
	
	public Vector<String> getCityName() throws Exception {
		Vector<String> cities = new Vector<String>();
		for(int i=0;i<citiesManager.getCitiesName().size();i++) {
			cities.add(citiesManager.getCitiesName().get(i).getCityName().toString());
		}
		return cities;
	}
	
	public Vector<String> getMostPopularDestinations() throws Exception {
		Vector<String> cities = new Vector<String>();
		for(int i=0;i<citiesManager.getCitiesName().size();i++) {
			if(citiesManager.getCitiesName().get(i).getPopularDestination() > 0) {
				cities.add(citiesManager.getCitiesName().get(i).getCityName().toString());
			}
		}
		return cities;
	}
}
