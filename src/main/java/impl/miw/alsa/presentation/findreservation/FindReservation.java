package impl.miw.alsa.presentation.findreservation;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.miw.alsa.model.Reservations;

public class FindReservation implements Validator {

	@SuppressWarnings("rawtypes")
	@Override
	public boolean supports(Class clazz) {
		
		return Reservations.class.equals(clazz);
	}
		
	@Override
	public void validate(Object obj, Errors err) {
		Reservations reservation = (Reservations) obj;

		if(reservation.getReservationCode().isEmpty() || reservation.getReservationCode() == null || reservation.getReservationCode().length() < 9 || reservation.getReservationCode().length() > 9) {
			err.rejectValue("reservationCode", "error.reservationcode");
		}	
	}
}