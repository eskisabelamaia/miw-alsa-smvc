package impl.miw.alsa.presentation.findreservation;

import impl.miw.alsa.business.reservation.ReservationsManager;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.miw.alsa.model.Reservations;

@Controller
public class FindreservationController {
	
	@Autowired
	private ReservationsManager reservationsManager;
	
	@RequestMapping(value = "/findReservation", method = RequestMethod.POST)
	public String findReservationPost(Model model, @ModelAttribute("reservation") Reservations reservation, BindingResult result, HttpSession session) throws Exception {
		FindReservation findReservationValidator = new FindReservation();
		findReservationValidator.validate(reservation, result);
		if(!result.hasErrors()) {
			model.addAttribute("ticket", reservation.getReservationCode());
			return "redirect:ticket";
		} else {
			return "findReservation";
		}
	}
	
	@RequestMapping(value = "/findReservation", method = RequestMethod.GET)
	public String findReservationGet(Model model, @ModelAttribute("reservation") Reservations reservation, RedirectAttributes redirectAttributes, BindingResult result, HttpSession session) throws NumberFormatException, Exception {
		String url = "findReservation";
		session.setAttribute("url", url);
		return url;
	}
}
