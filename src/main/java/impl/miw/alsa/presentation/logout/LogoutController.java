package impl.miw.alsa.presentation.logout;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.miw.alsa.model.UsersAccount;

@Controller
public class LogoutController {

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logoutGet(Model model, @ModelAttribute("login") UsersAccount account, BindingResult result, HttpSession session) {
		session.removeAttribute("user");
		session.invalidate();
		return "redirect:/";
	}
}
