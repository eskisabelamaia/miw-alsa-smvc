package impl.miw.alsa.presentation.ticket;

import impl.miw.alsa.business.reservation.ReservationsManager;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.miw.alsa.model.Reservations;

@Controller
public class TicketController {
	
	@Autowired
	private ReservationsManager reservationsManager;
				
	@RequestMapping(value = "/ticket", method = RequestMethod.POST)
	public String ticketPost(Model model, @ModelAttribute("reservation") Reservations reservation, BindingResult result, HttpSession session) throws Exception {
		if (reservationsManager.deleteReservationByReservationCode(session.getAttribute("reservationcode").toString())) {
			session.removeAttribute("reservationcode");
			return "redirect:/";
		} else {
			return "redirect:findReservation";
		}
	}
	
	@RequestMapping(value = "/ticket", method = RequestMethod.GET)
	public String ticketGet(Model model, @ModelAttribute("ticket") String reservationcode, @ModelAttribute("reservation") Reservations reservation,  BindingResult result, HttpSession session) throws NumberFormatException, Exception {
		if(session.getAttribute("usersname") == null && !session.getAttribute("url").equals("findReservation")) {
			return "redirect:login";
		} else {
			model.addAttribute("reservationinf", reservationsManager.getReservationByReservationCode(reservationcode));
			session.setAttribute("reservationcode", reservationcode);
			return "ticket";
		}
	}
}
