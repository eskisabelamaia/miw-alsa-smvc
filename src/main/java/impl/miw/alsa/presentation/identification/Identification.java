package impl.miw.alsa.presentation.identification;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.miw.alsa.model.UsersAccount;

public class Identification implements Validator {

	@SuppressWarnings("rawtypes")
	@Override
	public boolean supports(Class clazz) {
		
		return UsersAccount.class.equals(clazz);
	}
	
	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	private static final String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})";
	private Pattern pattern;
	private Matcher matcher;
	
	@Override
	public void validate(Object obj, Errors err) {
		UsersAccount useraccount = (UsersAccount) obj;

		if(useraccount.getUsername().isEmpty() && useraccount.getPassword().isEmpty()) {
			err.rejectValue("username", "error.username");
			err.rejectValue("password", "error.password");
		} else {
			if(useraccount.getUsername().isEmpty()) {
				err.rejectValue("username", "error.username");
			} else {
				pattern = Pattern.compile(EMAIL_PATTERN);
				matcher = pattern.matcher(useraccount.getUsername().toString());
				if(!matcher.matches()) {
					err.rejectValue("username", "error.username");
				}
			}
			if(useraccount.getPassword().isEmpty()) {
				err.rejectValue("password", "error.password");
			} else {
				pattern = Pattern.compile(PASSWORD_PATTERN);
				matcher = pattern.matcher(useraccount.getPassword().toString());
				if(!matcher.matches()) {
					err.rejectValue("password", "error.password");
				}
			}
		}
		
	}
}
