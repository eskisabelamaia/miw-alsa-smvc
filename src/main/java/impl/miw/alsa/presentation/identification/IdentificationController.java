package impl.miw.alsa.presentation.identification;

import impl.miw.alsa.business.usersaccount.UsersAccountManager;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.miw.alsa.model.UsersAccount;

@Controller
public class IdentificationController {

	@Autowired
	private UsersAccountManager usersaccountManager;
	
	public UsersAccountManager getUsersAccountManager() {
		return usersaccountManager;
	}
	
	public void setUsersAccountManager(UsersAccountManager usersaccountManager) {
		this.usersaccountManager = usersaccountManager;
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String loginPost(Model model, @ModelAttribute("login") UsersAccount account, BindingResult result, HttpSession session) {
		Identification identificationValidator = new Identification();
		identificationValidator.validate(account, result);
		if (result.hasErrors()) {
			return "login";
		} else {
			try {
				if(!usersaccountManager.verifyAccount(account)){
					result.reject("error.login");
					return "login";
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return "login";
			}
		}
		session.setAttribute("usersname", account.getUsername());
		String url = session.getAttribute("url").toString();
		session.removeAttribute("url");
		return "redirect:"+url;
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String loginGet(Model model, @ModelAttribute("login") UsersAccount account, BindingResult result) {
		return "login";
	}
}
