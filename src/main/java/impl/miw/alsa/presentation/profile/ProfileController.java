package impl.miw.alsa.presentation.profile;

import impl.miw.alsa.business.reservation.ReservationsManager;
import impl.miw.alsa.business.users.UsersManager;
import impl.miw.alsa.business.usersaccount.UsersAccountManager;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.miw.alsa.model.UsersAccount;

@Controller
public class ProfileController {

	@Autowired
	private ReservationsManager reservationsManager;
	
	@Autowired
	private UsersManager usersManager;
	
	@Autowired
	private UsersAccountManager usersAccountManager;
		
	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	public String profileGet(Model model, @ModelAttribute("profile") UsersAccount account, BindingResult result, HttpSession session) throws Exception {
		if(session.getAttribute("usersname") == null) {
			return "redirect:login";
		} else {
			int idaccount = usersAccountManager.getAccountByUsername(session.getAttribute("usersname").toString());
			int iduser = -1;
			iduser = usersManager.getUserByIdAccount(idaccount);
			if(iduser == -1) {
				return "alsa";
			} else {
				model.addAttribute("reservations", reservationsManager.getReservationsByUserId(iduser));
				return "profile";
			}
		}
	}
}
