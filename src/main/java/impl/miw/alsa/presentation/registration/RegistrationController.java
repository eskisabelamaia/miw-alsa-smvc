package impl.miw.alsa.presentation.registration;

import impl.miw.alsa.business.usersaccount.UsersAccountManager;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.miw.alsa.model.UsersAccount;

@Controller
public class RegistrationController {

	@Autowired
	private UsersAccountManager usersaccountManager;
	
	public UsersAccountManager getUsersAccountManager() {
		return usersaccountManager;
	}
	
	public void setUsersAccountManager(UsersAccountManager usersaccountManager) {
		this.usersaccountManager = usersaccountManager;
	}
		
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String registrationPost(Model model, @Valid @ModelAttribute("register") UsersAccount account, BindingResult result, HttpSession session) throws Exception {
		Registration registrationValidator = new Registration();
		registrationValidator.validate(account, result);
		if(usersaccountManager.verifyAccount(account)){
			result.reject("error.register");
			return "register";
		} else {
			if (result.hasErrors()) {
				return "register";
			} try {
				if (this.usersaccountManager.createUsersAccount(account)) {
					session.setAttribute("usersname", account.getUsername().toString());
					String url = session.getAttribute("url").toString();
					session.removeAttribute("url");
					return "redirect:" + url;
				} else {
					return "register";
				}
			} catch (Exception e) {
				result.reject("error.saveuser");
				e.printStackTrace();
				return "register";
			}
		}
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String registrationGet(Model model, @ModelAttribute("register") UsersAccount account, BindingResult result) {
		return "register";
	}
}
