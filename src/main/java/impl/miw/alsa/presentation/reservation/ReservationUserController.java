package impl.miw.alsa.presentation.reservation;

import impl.miw.alsa.business.cities.CitiesManager;
import impl.miw.alsa.business.reservation.ReservationsManager;
import impl.miw.alsa.business.route.RoutesManager;
import impl.miw.alsa.business.users.UsersManager;
import impl.miw.alsa.business.usersaccount.UsersAccountManager;

import java.util.Vector;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.miw.alsa.model.Users;

@Controller
@SessionAttributes("reservation")
public class ReservationUserController {
	
	@Autowired
	private ReservationsManager reservationsManager;
	
	@Autowired
	private RoutesManager routesManager;
	
	@Autowired
	private CitiesManager citiesManager;
	
	@Autowired
	private UsersManager usersManager;
	
	@Autowired
	private UsersAccountManager usersAccountManager;
	
	private Vector<String> docType;
		
	@RequestMapping(value = "/reservation_user", method = RequestMethod.POST)
	public String reservation_userPost(Model model, @Valid @ModelAttribute("user") Users user, BindingResult result, HttpSession session) {
		ReservationUser reservationUserValidator = new ReservationUser();
		reservationUserValidator.validate(user, result);
		if (!result.hasErrors()){
			if(!user.getMiddleName().isEmpty()) {
				String expression = "^[a-zA-Z\\s]+"; 
			    if(!user.getMiddleName().matches(expression)) {
			    	model.addAttribute("docType",docType);
			    	return "reservation_user";
			    } else {
			    	user.setMiddleName(user.getMiddleName());
			    }
			} else {
				user.setMiddleName("");
			}
			user.setFirstName(user.getFirstName());
			user.setSurname(user.getSurname());
			user.setIndetificationDocument(user.getIndetificationDocument());
			user.setIdentificationNumber(user.getIdentificationNumber());
			session.setAttribute("user", user);

			return "redirect:reservation";
		} else {
			model.addAttribute("docType",docType);
			return "reservation_user";
		}
	}
	
	@RequestMapping(value = "/reservation_user", method = RequestMethod.GET)
	public String reservation_userGet(Model model, @ModelAttribute("user") Users user, BindingResult result, HttpSession session) {
		if(session.getAttribute("usersname") == null) {
			return "redirect:login";
		} else {
			docType = getIdentificationType();
			model.addAttribute("docType",docType);
			return "reservation_user";
		}

	}
	
	private Vector<String> getIdentificationType() {
		Vector<String> docType = new Vector<String>();
		docType.add("DNI");
		docType.add("Passport");
		docType.add("NIE");
		return docType;
	}
}
