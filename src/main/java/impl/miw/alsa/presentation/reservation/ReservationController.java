package impl.miw.alsa.presentation.reservation;

import impl.miw.alsa.business.cities.CitiesManager;
import impl.miw.alsa.business.reservation.ReservationsManager;
import impl.miw.alsa.business.route.RoutesManager;
import impl.miw.alsa.business.users.UsersManager;
import impl.miw.alsa.business.usersaccount.UsersAccountManager;

import java.util.Random;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.miw.alsa.model.Reservations;
import com.miw.alsa.model.Routes;
import com.miw.alsa.model.Users;

@Controller
@SessionAttributes("reservation")
public class ReservationController {
	
	@Autowired
	private ReservationsManager reservationsManager;
	
	@Autowired
	private RoutesManager routesManager;
	
	@Autowired
	private CitiesManager citiesManager;
	
	@Autowired
	private UsersManager usersManager;
	
	@Autowired
	private UsersAccountManager usersAccountManager;

	@RequestMapping(value = "/reservation", method = RequestMethod.POST)
	public String reservationPost(Model model, @ModelAttribute("reservation") Reservations reservation, BindingResult result, HttpSession session) {
		Reservation reservationValidator = new Reservation();
		reservation.setSeats(reservation.getSeats());
		System.out.println("S:"+reservation.getSeats().toString());
		reservationValidator.validate(reservation, result);
		if(!result.hasErrors()) {
			reservation.setAdditionalOpt(reservation.getAdditionalOpt());
			reservation.setHasInsurance(reservation.isHasInsurance());
			double finalPrice = reservation.getPrice();
			if(reservation.isHasInsurance()) {
				finalPrice = finalPrice + 5.0;
			}
			if(!reservation.getAdditionalOpt().equals("none")) {
				finalPrice = finalPrice + 7.5;
			}
			reservation.setPrice(finalPrice);
			return "redirect:resume";
		} else {
			return "reservation";
		}
	}
	
	@RequestMapping(value = "/reservation", method = RequestMethod.GET)
	public String reservationGet(Model model, @ModelAttribute("reservation") Reservations reservation, BindingResult result, HttpSession session) {
		if(session.getAttribute("usersname") == null) {
			return "redirect:login";
		} else {
			return "reservation";
		}
	}
	
	@RequestMapping(value = "/resume", method = RequestMethod.POST)
	public String resumePost(Model model, @ModelAttribute("reservation") Reservations reservation, @ModelAttribute("user") Users user,  BindingResult result, HttpSession session) throws Exception {
		int iduser = 0;
		int idaccount = usersAccountManager.getAccountByUsername(session.getAttribute("usersname").toString());
		System.out.println("IdAccount: "+idaccount);
		user = (Users) session.getAttribute("user");
		if (usersManager.createUser(user.getFirstName(), user.getMiddleName(), user.getSurname(), user.getIndetificationDocument(), user.getIdentificationNumber(), idaccount)) {
			iduser = usersManager.getUserByIdAccount(idaccount);
		} else {
			model.addAttribute("route", session.getAttribute("route"));
			model.addAttribute("user", session.getAttribute("user"));
			model.addAttribute("reservation", session.getAttribute("reservation"));
			return "reservation";
		}
		
		if(reservationsManager.createReservation(reservation.getReservationCode(), reservation.getDepartureDate(), reservation.getReturnDate(), 
				reservation.getTraveltype(), reservation.getNumberSeats(), reservation.getSeats(), 
				reservation.getAdditionalOpt(), reservation.isHasInsurance(), iduser, reservation.getRouteID(), 
				reservation.getDepartureCity(), reservation.getDestinationCity(), reservation.getPrice())) {
			citiesManager.addCounterPopularDestination(reservation.getDestinationCity());
		} else {
			return "redirect: error";
		}
		return "redirect:gratitude";
		
	}
	
	@RequestMapping(value = "/resume", method = RequestMethod.GET)
	public String resumeGet(Model model, @ModelAttribute("reservation") Reservations reservation, @ModelAttribute("user") Users user, @ModelAttribute("routes") Routes route, BindingResult result, HttpSession session) throws NumberFormatException, Exception {
		if(session.getAttribute("usersname") == null) {
			return "redirect:login";
		} else {
			String referenceCode = getReferenceCode();
			reservation.setReservationCode(referenceCode);
			model.addAttribute("route", session.getAttribute("route"));
			model.addAttribute("user", session.getAttribute("user"));
			model.addAttribute("reservation", session.getAttribute("reservation"));
			return "resume";
		}
	}
	
	@RequestMapping(value = "/error", method = RequestMethod.GET)
	public String errorGet(Model model, @ModelAttribute("reservation") Reservations reservation, BindingResult result, HttpSession session) throws NumberFormatException, Exception {
		if(session.getAttribute("usersname") == null) {
			return "redirect:login";
		} else {
			return "error";
		}
	}
	
	@RequestMapping(value = "/gratitude", method = RequestMethod.GET)
	public String gratitudeGet(Model model, @ModelAttribute("reservation") Reservations reservation, BindingResult result, HttpSession session) throws NumberFormatException, Exception {
		if(session.getAttribute("usersname") == null) {
			return "redirect:login";
		} else {
			return "gratitude";
		}
	}
		
	private String getReferenceCode() {
		String cadenaAleatoria = "";
		long milis = new java.util.GregorianCalendar().getTimeInMillis();
		Random r = new Random(milis);
		int longitud = 9;
		int i = 0;
		while ( i < longitud){
			char c = (char)r.nextInt(255);
			if ( (c >= '0' && c <='9') || (c >='A' && c <='Z') ){
				cadenaAleatoria += c;
				i ++;
			}
		}
		return cadenaAleatoria;
	}
}
