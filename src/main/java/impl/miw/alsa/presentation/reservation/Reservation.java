package impl.miw.alsa.presentation.reservation;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.miw.alsa.model.Reservations;

public class Reservation implements Validator{

	@SuppressWarnings("rawtypes")
	@Override
	public boolean supports(Class clazz) {
		
		return Reservations.class.equals(clazz);
	}
		 
	@Override
	public void validate(Object obj, Errors err) {
		Reservations reservation = (Reservations) obj;
		
		if(reservation.getSeats().isEmpty() || reservation.getSeats() == null) {
			err.rejectValue("seats", "error.seats");
		} else {
			String[] strNumbers = reservation.getSeats().split(",");
			System.out.println(strNumbers.length);
			System.out.println(reservation.getNumberSeats());
			if(reservation.getNumberSeats() != strNumbers.length) {
				err.rejectValue("seats", "error.seats");
			}
		}
		
		if(reservation.getAdditionalOpt().isEmpty() || reservation.getAdditionalOpt() == null) {
			err.rejectValue("additionalOpt", "error.additionalOpt");
		}
	}
}
