package impl.miw.alsa.presentation.reservation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.miw.alsa.model.Users;

public class ReservationUser implements Validator{

	@SuppressWarnings("rawtypes")
	@Override
	public boolean supports(Class clazz) {
		
		return Users.class.equals(clazz);
	}
		 
	@Override
	public void validate(Object obj, Errors err) {
		Users user = (Users) obj;

		if(user.getFirstName().isEmpty() && user.getSurname().isEmpty() && user.getIdentificationNumber().isEmpty() ) {
			err.rejectValue("firstName", "error.firstName");
			err.rejectValue("surname", "error.surname");
			err.rejectValue("identificationNumber", "error.identificationNumber");
		} else {
			if(user.getFirstName().isEmpty() || user.getFirstName() == null) {
				err.rejectValue("firstName", "error.firstName");
			} else {
				String expression = "^[a-zA-Z\\s]+"; 
			    if(!user.getFirstName().matches(expression)) {
			    	err.rejectValue("firstName", "error.firstName");
			    }     
			}
			if(user.getSurname().isEmpty() || user.getSurname() == null) {
				err.rejectValue("surname", "error.surname");
				System.out.println("username incorrect");
			} else {
				String expression = "[a-zA-z]+([ '-][a-zA-Z]+)*"; 
			    if(!user.getSurname().matches(expression)) {
			    	err.rejectValue("surname", "error.firstName");
			    }     
			}
			if(user.getIdentificationNumber().isEmpty() || user.getIdentificationNumber() == null) {
				err.rejectValue("identificationNumber", "error.identificationNumber");
			} else {
				switch(user.getIndetificationDocument().toString()) {
				case "DNI":
					validateDNI(user.getIdentificationNumber().toString(), err);
					break;
				case "Passport": validatePassport(user, err);
					break;
				case "NIE": validateNIE(user, err);
					break;
				}
			}
		}
	}
	
	public void validateDNI(String number, Errors err) {
	    Pattern pattern = Pattern.compile("(\\d{1,8})([TRWAGMYFPDXBNJZSQVHLCKEtrwagmyfpdxbnjzsqvhlcke])");
	    Matcher matcher = pattern.matcher(number);
	    if (matcher.matches()) {
	    	matcher.reset();
	    	if(matcher.find()){
		        String letter = matcher.group(2);
		        String letters = "TRWAGMYFPDXBNJZSQVHLCKE";
		        int index = Integer.parseInt(matcher.group(1));
		        index = index % 23;
		        String reference = letters.substring(index, index + 1);
		        System.out.println(reference);
		        if (!reference.equalsIgnoreCase(letter)) {
		        	err.rejectValue("identificationNumber", "error.identificationNumber");
		        }
	    	}
	    }else {
        	err.rejectValue("identificationNumber", "error.identificationNumber");
	    }
	}
	public void validatePassport(Users user, Errors err) {
		int lengthPassport = 8;
		if(lengthPassport != user.getIdentificationNumber().length()) {
			err.rejectValue("identificationNumber", "error.identificationNumber");
		}
	}
	public void validateNIE(Users user, Errors err) {
	    String letter = "";
	    String dni = "";
	 
	    if (user.getIdentificationNumber().length() < 9) {
	    	err.rejectValue("identificationNumber", "error.identificationNumber");
	    } else {
		    letter = user.getIdentificationNumber().substring(0, 1); 
		    if ((letter.equals("X")) || (letter.equals("K")) || (letter.equals("L")) || (letter.equals("M"))) {
		        dni = user.getIdentificationNumber().substring(1, user.getIdentificationNumber().length());
		    } else {
		    	err.rejectValue("identificationNumber", "error.identificationNumber");
		    }
		    validateDNI(dni, err);
		} 
	}
}
