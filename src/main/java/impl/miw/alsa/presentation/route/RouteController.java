package impl.miw.alsa.presentation.route;

import java.util.Vector;

import impl.miw.alsa.business.cities.CitiesManager;
import impl.miw.alsa.business.route.RoutesManager;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.miw.alsa.model.Reservations;
import com.miw.alsa.model.Routes;

@Controller
@SessionAttributes("reservation")
public class RouteController {

	@Autowired
	private RoutesManager routesManager;
	
	@Autowired
	private CitiesManager citiesManager;

	@RequestMapping(value = "/routes", method = RequestMethod.POST)
	public String routesPost(Model model, @ModelAttribute("routes") Routes route, @ModelAttribute("reservation") Reservations reservation, RedirectAttributes redirectAttributes, BindingResult result, HttpSession session) throws Exception {	
		reservation.setRouteID(route.getId());
		route = routesManager.getRouteById(route.getId());
		if(reservation.getTraveltype().equals("roundtrip")) {
			reservation.setPrice(route.getReturnPrice());
		} else {
			reservation.setPrice(route.getOnewayPrice());
		}
		session.setAttribute("route", route);
		if(session.getAttribute("user") == null) {
			String url ="reservation_user";
			session.setAttribute("url", url);
			return "redirect:login";
		} else {
			return "redirect:reservation_user";
		}
	}
	
	@RequestMapping(value = "/routes", method = RequestMethod.GET)
	public String routesGet(Model model, @ModelAttribute("routes") Routes route, @ModelAttribute("reservation") Reservations reservation, BindingResult result, HttpSession session) throws Exception {
		int departure = citiesManager.getCityByName(reservation.getDepartureCity());
		int arrival = citiesManager.getCityByName(reservation.getDestinationCity());
		Vector <Routes> routes = routesManager.getRoutes(departure, arrival);
		route.setId(routes.get(0).getId());
		model.addAttribute("routestable", routes);
		model.addAttribute("reservation", reservation);
		return "routes";
	}
}
