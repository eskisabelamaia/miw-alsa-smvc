package impl.miw.alsa.presentation.counter;

public class VisitorCounter {
	private static VisitorCounter instance;
	private int counter=0;
		
	private VisitorCounter(){}
		
	public static VisitorCounter getInstance(){
		if (instance == null){
			instance = new VisitorCounter();
		}
		return instance;
	}
		
	public Integer inc(){
		return ++instance.counter;
	}
}
