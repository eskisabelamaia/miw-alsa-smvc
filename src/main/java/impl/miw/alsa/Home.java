package impl.miw.alsa;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.miw.alsa.model.Reservations;

public class Home implements Validator{

	@SuppressWarnings("rawtypes")
	@Override
	public boolean supports(Class clazz) {
		
		return Reservations.class.equals(clazz);
	}
	
	private Pattern pattern;
	private Matcher matcher;
	
	private static final String DATE_PATTERN = "(0?[1-9]|[12][0-9]|3[01])-(0?[1-9]|1[012])-((19|20)\\d\\d)";
	 
	@Override
	public void validate(Object obj, Errors err) {
		Reservations reservation = (Reservations) obj;
				
		if(reservation.getDepartureCity().equals(reservation.getDestinationCity())) {
			err.rejectValue("destinationCity", "error.samecity");
		}
		
		if(reservation.getTraveltype() == null || reservation.getTraveltype().isEmpty()) {
			err.rejectValue("traveltype", "error.traveltypenull");
			if(validDate(reservation.getDepartureDate().toString()) == false) {
				err.rejectValue("departureDate", "error.dateformat");
			}
			if(validDate(reservation.getReturnDate().toString()) == false) {
				err.rejectValue("returnDate", "error.dateformat");
			}
		} else {
			switch(reservation.getTraveltype().toString()) {
			case "roundtrip": validateRoundtrip(reservation, err);
				break;
			case "oneway": validateOneway(reservation, err);
				break;
			case "openreturn": validateOpenreturn(reservation, err);
				break;
			}
		}
	}
	
	public void validateRoundtrip (Reservations reservation, Errors err) {
		ValidationUtils.rejectIfEmptyOrWhitespace(err, "departureDate", "error.dateformat");
		if(reservation.getDepartureDate() == null || reservation.getDepartureDate().isEmpty()) {
			err.rejectValue("departureDate", "error.dateformat");
		} else {
			if(validDate(reservation.getDepartureDate().toString()) == false) {
				err.rejectValue("departureDate", "error.dateformat");
			}
		}
		ValidationUtils.rejectIfEmptyOrWhitespace(err, "returnDate", "error.dateformat");
		if(reservation.getReturnDate() == null || reservation.getReturnDate().isEmpty()) {
			err.rejectValue("returnDate", "error.dateformat");
		} else {
			if(validDate(reservation.getReturnDate().toString()) == false) {
				err.rejectValue("returnDate", "error.dateformat");
			} else {
				validateReturnDate(reservation.getDepartureDate().toString(),reservation.getReturnDate().toString(), err);
			}
		}
	}
	
	public void validateReturnDate(String departureDate, String returnDate, Errors err) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
		try {
	 
			Date departureDate1 = formatter.parse(departureDate);
			Date returnDate1 = formatter.parse(returnDate);
			
			if(returnDate1.before(departureDate1)){
				err.rejectValue("returnDate", "error.returndatevalue");
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	public void validateOneway (Reservations reservation, Errors err) {
		ValidationUtils.rejectIfEmptyOrWhitespace(err, "departureDate", "error.dateformat");
		if(reservation.getDepartureDate() == null || reservation.getDepartureDate().isEmpty()) {
			err.rejectValue("departureDate", "error.dateformat");
		} else {
			if(validDate(reservation.getDepartureDate().toString()) == false) {
				err.rejectValue("departureDate", "error.dateformat");
			}
		}
	}
	
	public void validateOpenreturn (Reservations reservation, Errors err) {
		ValidationUtils.rejectIfEmptyOrWhitespace(err, "departureDate", "error.dateformat");
		if(reservation.getDepartureDate() == null || reservation.getDepartureDate().isEmpty()) {
			err.rejectValue("departureDate", "error.dateformat");
			System.out.println("Fecha de salida vacio");
		} else {
			if(validDate(reservation.getDepartureDate().toString()) == false) {
				err.rejectValue("departureDate", "error.dateformat");
				System.out.println("Fecha de salida erroneo");
			}
		}
	}

	public boolean validDate (String date) {
	
		pattern = Pattern.compile(DATE_PATTERN);
		matcher = pattern.matcher(date);
		
		if(matcher.matches()){
			matcher.reset();
			if(matcher.find()){
				String day = matcher.group(1);
				String month = matcher.group(2);
				int year = Integer.parseInt(matcher.group(3));
				if (day.equals("31") && (month.equals("4") || month .equals("6") || month.equals("9") || 
						month.equals("11") || month.equals("04") || month .equals("06") || month.equals("09"))) {
					return false;
				} else if (month.equals("2") || month.equals("02")) {
					  if(year % 4==0){
						  if(day.equals("30") || day.equals("31")){
							  return false;
						  }else{
							  return true;
						  }
					  }else{
					         if(day.equals("29")||day.equals("30")||day.equals("31")){
					        	 return false;
					         }else{
							  return true;
						  }
					  }
				}else{				 
					return true;				 
				}
			}else{
				return false;
			}
		}else{
			return false;
		}	

	}
}
