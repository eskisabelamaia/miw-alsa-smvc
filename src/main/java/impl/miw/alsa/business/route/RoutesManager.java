package impl.miw.alsa.business.route;

import java.util.Vector;

import org.springframework.beans.factory.annotation.Autowired;

import com.miw.alsa.business.RouteManagerService;
import com.miw.alsa.model.Routes;
import com.miw.alsa.persistence.RouteDataService;

public class RoutesManager implements RouteManagerService {

	@Autowired
	private RouteDataService routeDataService;

	public void setRouteDataService(RouteDataService routeDataService) {
		this.routeDataService = routeDataService;
	}
	@Override
	public Routes getRouteById(Integer id) throws Exception {
		return this.routeDataService.getRouteById(id);
	}

	@Override
	public Vector<Routes> getRoutes(int departure, int arrival) throws Exception {
		return this.routeDataService.getRoutes(departure, arrival);
	}
	@Override
	public int getRouteIdByCityId(int departure, int arrival) throws Exception {
		return this.routeDataService.getRouteIdByCityId(departure, arrival);
	}
}
