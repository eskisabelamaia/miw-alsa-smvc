package impl.miw.alsa.business.reservation;

import java.util.Vector;

import org.springframework.beans.factory.annotation.Autowired;

import com.miw.alsa.business.ReservationsManagerService;
import com.miw.alsa.model.Reservations;
import com.miw.alsa.persistence.ReservationsDataService;


public class ReservationsManager implements ReservationsManagerService {

	@Autowired
	private ReservationsDataService ReservationsDataService;

	@Override
	public Vector<Reservations> getReservationsByUserId(int idUser)
			throws Exception {
		return ReservationsDataService.getReservationsByUserId(idUser);
	}

	@Override
	public Reservations getReservationByReservationCode(String reservationCode)
			throws Exception {
		return ReservationsDataService.getReservationByReservationCode(reservationCode);
	}

	@Override
	public boolean createReservation(String reservationcode,
			String departuredate, String returndate, String traveltype,
			int numberseats, String seats, String additionalopt,
			boolean hasinsurance, int iduser, int idroute,
			String citydeparture, String citydestination, double price)
			throws Exception {
		// TODO Auto-generated method stub
		return ReservationsDataService.createReservation(reservationcode,departuredate,returndate,traveltype,numberseats,seats,
				additionalopt,hasinsurance,iduser,idroute,citydeparture,citydestination,price);
	}

	@Override
	public boolean deleteReservationByReservationCode(String reservationCode)
			throws Exception {
		// TODO Auto-generated method stub
		return ReservationsDataService.deleteReservationByReservationCode(reservationCode);
	}
}
