package impl.miw.alsa.persistence.reservation;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Vector;

import com.miw.alsa.model.Reservations;
import com.miw.alsa.persistence.ReservationsDataService;


/**
 * Clase que implementa las operaciones de acceso a la base de datos para la
 * entidad TIME.
 * 
 */
public class ReservationsDAO implements ReservationsDataService {

	@Override
	public Vector<Reservations> getReservationsByUserId(int idUser)
			throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;
		
		Vector<Reservations> vctReservations = new Vector<Reservations>();

		try {
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost/alsa";

			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "admin", "alsa");
			
			ps = con.prepareStatement("select * from reservations where iduser=?");
			ps.setInt(1, idUser);
			rs = ps.executeQuery();
			while (rs.next()) {
				Reservations reservation = new Reservations(rs.getInt("id_reservation"), rs.getString("reservationcode"), rs.getInt("iduser"), rs.getString("additionalopt"), rs.getBoolean("hasinsurance"), rs.getDouble("price"),
						rs.getInt("idroute"), rs.getString("citydeparture"), rs.getString("citydestination"), rs.getString("departuredate"), rs.getString("returndate"), rs.getString("traveltype"), rs.getInt("numberseats"), 
						rs.getString("seats"));
				vctReservations.add(reservation);
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw (e);
		} finally {
			try {
				ps.close();
				con.close();
			} catch (Exception e) {
			}
		}
		return vctReservations;
	}

	@Override
	public Reservations getReservationByReservationCode(String reservationCode) throws Exception {
	PreparedStatement ps = null;
	ResultSet rs = null;
	Connection con = null;
	
	Reservations reservation = null;
	
	try {
		String SQL_DRV = "org.hsqldb.jdbcDriver";
		String SQL_URL = "jdbc:hsqldb:hsql://localhost/alsa";

		Class.forName(SQL_DRV);
		con = DriverManager.getConnection(SQL_URL, "admin", "alsa");
		
		ps = con.prepareStatement("select * from reservations where reservationcode=?");
		ps.setString(1, reservationCode);
		rs = ps.executeQuery();
		while (rs.next()) {
			reservation = new Reservations(rs.getInt("id_reservation"), rs.getString("reservationcode"), rs.getInt("iduser"), rs.getString("additionalopt"), rs.getBoolean("hasinsurance"), rs.getDouble("price"),
				rs.getInt("idroute"), rs.getString("citydeparture"), rs.getString("citydestination"), rs.getString("departuredate"), rs.getString("returndate"), rs.getString("traveltype"), rs.getInt("numberseats"), 
				rs.getString("seats"));
		}

	} catch (Exception e) {
		e.printStackTrace();
		throw (e);
	} finally {
		try {
			ps.close();
			con.close();
		} catch (Exception e) {
		}
	}
	System.out.println(reservation);
	return reservation;
	}

	@Override
	public boolean createReservation(String reservationcode, String departuredate, String returndate, String traveltype, int numberseats, String seats, String additionalopt, boolean hasinsurance, int iduser, int idroute, String citydeparture, String citydestination, double price) throws Exception {
		PreparedStatement ps = null;
		Connection con = null;

		try {
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost/alsa";

			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "admin", "alsa");
			
			java.sql.Date deptDate = parseStringtoDate(departuredate);
			java.sql.Date retDate = parseStringtoDate(returndate);
						
			ps = con.prepareStatement("insert into reservations(reservationcode,departuredate,returndate,traveltype,numberseats,seats,additionalopt,hasinsurance,iduser,idroute,citydeparture,citydestination,price) values (?,?,?,?,?,?,?,?,?,?,?,?,?)");
			ps.setString(1, reservationcode);
			ps.setDate(2, deptDate);
			ps.setDate(3, retDate);
			ps.setString(4, traveltype);
			ps.setInt(5, numberseats);
			ps.setString(6, seats);
			ps.setString(7, additionalopt);
			ps.setBoolean(8, hasinsurance);
			ps.setInt(9, iduser);
			ps.setInt(10, idroute);
			ps.setString(11, citydeparture);
			ps.setString(12, citydestination);
			ps.setDouble(13, price);
			ps.executeUpdate();

		}
		catch (SQLException e) {
			System.out.println(e);
			return false;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw (e);
			
		} finally {
			try {
				ps.close();
				con.close();
			} catch (Exception e) {
			}
		}
		return true;
	}
	
	public java.sql.Date parseStringtoDate(String strDate) throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        java.util.Date parsed = format.parse(strDate);
        java.sql.Date sql = new java.sql.Date(parsed.getTime());
        return sql;
	}

	@Override
	public boolean deleteReservationByReservationCode(String reservationCode) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;

		boolean deleted = true;
		try {
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost/alsa";

			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "admin", "alsa");
			
			ps = con.prepareStatement("delete from reservations where reservationcode=?");
			ps.setString(1, reservationCode);
			ps.executeUpdate();
			ps.close();
			
			ps = con.prepareStatement("select * from reservations where reservationcode=?");
			ps.setString(1, reservationCode);
			rs = ps.executeQuery();
			while (rs.next()) {
				deleted = false;
			}

		}
		catch (SQLException e) {
			System.out.println(e);
			deleted = true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw (e);
			
		} finally {
			try {
				ps.close();
				con.close();
			} catch (Exception e) {
			}
		}
		return deleted;
	}

}