package impl.miw.alsa.persistence.usersaccount;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.miw.alsa.model.UsersAccount;
import com.miw.alsa.persistence.UsersAccountDataService;

public class UsersAccountDAO implements UsersAccountDataService {

	@Override
	public boolean createUsersAccount(UsersAccount account) throws Exception {
		PreparedStatement ps = null;
		Connection con = null;

		try {
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost/alsa";

			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "admin", "alsa");

			ps = con.prepareStatement("insert into usersaccounts(username,password) values (?,?)");
			ps.setString(1, account.getUsername());
			ps.setString(2, account.getPassword());
			ps.executeUpdate();

		}
		catch (SQLException e) {
			System.out.println(e);
			return false;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw (e);
			
		} finally {
			try {
				ps.close();
				con.close();
			} catch (Exception e) {
			}
		}
		return true;
	}
	
	@Override
	public boolean verifyAccount(UsersAccount account) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;

		try {
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost/alsa";

			// Obtenemos la conexi�n a la base de datos.
			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "admin", "alsa");

			ps = con.prepareStatement("select username, password from usersaccounts where username=?");
			ps.setString(1, account.getUsername());
			rs = ps.executeQuery();
			while (rs.next()) {
				if (account.getPassword().equals(rs.getString("password"))){ return true; }else{ return false; }
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw (e);
		} finally {
			try {
				ps.close();
				con.close();
			} catch (Exception e) {
			}
		}

		return false;
	}

	
	@Override
	public int getAccountByUsername(String username) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;
		
		int idaccount = 0;

		try {
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost/alsa";

			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "admin", "alsa");

			ps = con.prepareStatement("select id_account from usersaccounts where username=?");
			ps.setString(1, username);
			rs = ps.executeQuery();
			while (rs.next()) {
				idaccount = (rs.getInt("id_account"));
			}

		}
		catch (SQLException e) {
			System.out.println(e);
			return -1;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw (e);
			
		} finally {
			try {
				ps.close();
				con.close();
			} catch (Exception e) {
			}
		}
		return idaccount;
	}
}