package impl.miw.alsa.persistence.route;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

import com.miw.alsa.model.Routes;
import com.miw.alsa.persistence.RouteDataService;

public class RoutesDAO implements RouteDataService {

	@Override
	public Routes getRouteById(Integer id) throws Exception {
		Routes route = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;

		try {
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost/alsa";
			
			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "admin", "alsa");
						
			ps = con.prepareStatement("select * from routes where id_route=?");
			ps.setInt(1, id);
			rs = ps.executeQuery();
			while (rs.next()) {
				route = new Routes(id,rs.getString("departure"), rs.getString("arrival"), rs.getString("destdeparture"), rs.getString("destarrival"), rs.getDouble("onewayprice"), rs.getDouble("returnprice"),rs.getInt("citydeparture"),rs.getInt("citydestination"));
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw (e);
		} finally {
			try {
				ps.close();
				con.close();
			} catch (Exception e) {
			}
		}
		return route;
	}

	@Override
	public Vector<Routes> getRoutes(int departure,int arrival) throws Exception {
		Vector<Routes> vctRoutes = new Vector<Routes>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;

		try {
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost/alsa";
			
			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "admin", "alsa");
						
			ps = con.prepareStatement("select * from routes where citydeparture=? and citydestination=?");
			ps.setInt(1, departure);
			ps.setInt(2, arrival);
			rs = ps.executeQuery();
			while (rs.next()) {
				Routes routes = new Routes(rs.getInt("id_route"),rs.getString("departure"), rs.getString("arrival"), rs.getString("destdeparture"), rs.getString("destarrival"),rs.getDouble("onewayprice"), rs.getDouble("returnprice"),rs.getInt("citydeparture"),rs.getInt("citydestination"));
				vctRoutes.add(routes);
				System.out.println(vctRoutes.get(0));
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw (e);
		} finally {
			try {
				ps.close();
				con.close();
			} catch (Exception e) {
			}
		}
		return vctRoutes;
	}
	
	@Override
	public int getRouteIdByCityId(int departure,int arrival) throws Exception {
		int idroute = 0;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;

		try {
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost/alsa";
			
			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "admin", "alsa");
						
			ps = con.prepareStatement("select * from routes where citydeparture=? and citydestination=?");
			ps.setInt(1, departure);
			ps.setInt(2, arrival);
			rs = ps.executeQuery();
			while (rs.next()) {
				System.out.println(rs.getInt("id_route"));
				idroute = rs.getInt("id_route");
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw (e);
		} finally {
			try {
				ps.close();
				con.close();
			} catch (Exception e) {
			}
		}
		return idroute;
	}
}
