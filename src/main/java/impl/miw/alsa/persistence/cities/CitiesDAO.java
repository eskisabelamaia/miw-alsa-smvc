package impl.miw.alsa.persistence.cities;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

import com.miw.alsa.model.Cities;
import com.miw.alsa.persistence.CitiesDataService;


/**
 * Clase que implementa las operaciones de acceso a la base de datos para la
 * entidad Cities.
 * 
 */
public class CitiesDAO implements CitiesDataService {
	
	@Override
	public int getCityByName (String cityname) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;

		int id = 0;
		
		try {
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost/alsa";
			
			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "admin", "alsa");
			
			ps = con.prepareStatement("select * from cities where cityname=?");
			ps.setString(1, cityname);
			rs = ps.executeQuery();
			while (rs.next()) {
				id = rs.getInt("id_city");
			}
		
		} catch (Exception e) {
			e.printStackTrace();
			throw (e);
		} finally {
			try {
				ps.close();
				con.close();
			} catch (Exception e) {
			}
		}
		return id;
	}

	@Override
	public Vector<Cities> getCitiesName() throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;
		
		Vector<Cities> vctCities = new Vector<Cities>();

		try {
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost/alsa";

			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "admin", "alsa");

			ps = con.prepareStatement("select id_city,cityname,populardestination from cities order by populardestination desc");
			rs = ps.executeQuery();
			while (rs.next()) {
				Cities cities = new Cities(rs.getInt("id_city"), rs.getString("cityname"), rs.getInt("populardestination"));
				vctCities.add(cities);
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw (e);
		} finally {
			try {
				ps.close();
				con.close();
			} catch (Exception e) {
			}
		}
		return vctCities;
	}

	@Override
	public void addCounterPopularDestination(String cityName) throws Exception {
		PreparedStatement ps = null;
		PreparedStatement ps2 = null;
		ResultSet rs = null;
		Connection con = null;

		int counter = -1;
		
		try {
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost/alsa";
			
			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "admin", "alsa");
			
			ps = con.prepareStatement("select populardestination from cities where cityname=?");
			ps.setString(1, cityName);
			rs = ps.executeQuery();
			while (rs.next()) {
				counter = rs.getInt("populardestination");
				System.out.println(counter);
			}
			 if(counter != -1) {
				 counter++;
			 }
			ps.close();
			
			ps2 = con.prepareStatement("update cities set populardestination=? where cityname =?");
			ps2.setInt(1, counter);
			ps2.setString(2, cityName);
			ps2.executeUpdate();
		
		} catch (Exception e) {
			e.printStackTrace();
			throw (e);
		} finally {
			try {
				ps2.close();
				con.close();
			} catch (Exception e) {
			}
		}
		
	}
}