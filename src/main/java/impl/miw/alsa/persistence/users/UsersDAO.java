package impl.miw.alsa.persistence.users;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.miw.alsa.persistence.UsersDataService;

/**
 * Clase que implementa las operaciones de acceso a la base de datos para la
 * entidad USER.
 * 
 */
public class UsersDAO implements UsersDataService {

	@Override
	public boolean createUser(String firstName, String middleName, String surname, String identificationType, String numIdentification, int idaccount) throws Exception {
		PreparedStatement ps = null;
		Connection con = null;

		try {
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost/alsa";

			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "admin", "alsa");

			ps = con.prepareStatement("insert into users(firstname,middlename,surname,identificationtype, identificationnumber,idaccount) values (?,?,?,?,?,?)");
			ps.setString(1, firstName);
			ps.setString(2, middleName);
			ps.setString(3, surname);
			ps.setString(3, identificationType);
			ps.setString(4, numIdentification);
			ps.setInt(5, idaccount);
			ps.executeUpdate();

		}
		catch (SQLException e) {
			System.out.println(e);
			return false;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw (e);
			
		} finally {
			try {
				ps.close();
				con.close();
			} catch (Exception e) {
			}
		}
		return true;
	}

	@Override
	public int getUserByIdAccount(int idaccount) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;
		
		int iduser = 0;

		try {
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost/alsa";

			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "admin", "alsa");

			ps = con.prepareStatement("select id_user from users where idaccount=?");
			ps.setInt(1, idaccount);
			rs = ps.executeQuery();
			while (rs.next()) {
				iduser = (rs.getInt("id_user"));
			}

		}
		catch (SQLException e) {
			System.out.println(e);
			return -1;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw (e);
			
		} finally {
			try {
				ps.close();
				con.close();
			} catch (Exception e) {
			}
		}
		return iduser;
	}
}