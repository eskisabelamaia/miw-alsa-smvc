package com.miw.alsa.model;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

public class Users {
	
	private int id;
	private int idaccount;
	private String indetificationDocument;
	@NotNull
	@NotEmpty
	private String firstName;
	private String middleName;
	private String surname;
	private String identificationNumber;
	
	public Users(){
		super();
	}

	public Users(int id, String firstName, String middleName, String surname,
			String indetificationDocument, String identificationNumber, int idaccount) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.middleName = middleName;
		this.surname = surname;
		this.indetificationDocument = indetificationDocument;
		this.identificationNumber = identificationNumber;
		this.idaccount = idaccount;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getIndetificationDocument() {
		return indetificationDocument;
	}

	public void setIndetificationDocument(String indetificationDocument) {
		this.indetificationDocument = indetificationDocument;
	}

	public String getIdentificationNumber() {
		return identificationNumber;
	}

	public void setIdentificationNumber(String identificationNumber) {
		this.identificationNumber = identificationNumber;
	}

	public int getIdaccount() {
		return idaccount;
	}

	public void setIdaccount(int idaccount) {
		this.idaccount = idaccount;
	}
}
