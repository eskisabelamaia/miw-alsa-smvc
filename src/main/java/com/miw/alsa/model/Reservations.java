package com.miw.alsa.model;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

public class Reservations {
	
	private int id;
	private String reservationCode;
	private int idUser;	
	private String additionalOpt;
	private boolean hasInsurance;
	private double price;
	private int routeID;
	private int numberSeats;
	private String seats;
	
	@NotNull
	@NotEmpty
	private String departureCity;
	private String destinationCity;
	private String departureDate;
	private String returnDate;
	private String traveltype;
		
	public Reservations (){
		super();
	}

	public Reservations(int id, String reservationCode, int idUser,
			String additionalOpt, boolean hasInsurance, double price,
			int routeID, String departureCity, String destinationCity,
			String departureDate, String returnDate, String traveltype,
			int numberSeats, String seats) {
		super();
		this.id = id;
		this.reservationCode = reservationCode;
		this.idUser = idUser;
		this.additionalOpt = additionalOpt;
		this.hasInsurance = hasInsurance;
		this.price = price;
		this.routeID = routeID;
		this.departureCity = departureCity;
		this.destinationCity = destinationCity;
		this.departureDate = departureDate;
		this.returnDate = returnDate;
		this.traveltype = traveltype;
		this.numberSeats = numberSeats;
		this.seats = seats;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getReservationCode() {
		return reservationCode;
	}

	public void setReservationCode(String reservationCode) {
		this.reservationCode = reservationCode;
	}

	public int getIdUser() {
		return idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public String getAdditionalOpt() {
		return additionalOpt;
	}

	public void setAdditionalOpt(String additionalOpt) {
		this.additionalOpt = additionalOpt;
	}

	public boolean isHasInsurance() {
		return hasInsurance;
	}

	public void setHasInsurance(boolean hasInsurance) {
		this.hasInsurance = hasInsurance;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getRouteID() {
		return routeID;
	}

	public void setRouteID(int routeID) {
		this.routeID = routeID;
	}

	public String getDepartureCity() {
		return departureCity;
	}

	public void setDepartureCity(String departureCity) {
		this.departureCity = departureCity;
	}

	public String getDestinationCity() {
		return destinationCity;
	}

	public void setDestinationCity(String destinationCity) {
		this.destinationCity = destinationCity;
	}

	public String getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}

	public String getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(String returnDate) {
		this.returnDate = returnDate;
	}

	public String getTraveltype() {
		return traveltype;
	}

	public void setTraveltype(String traveltype) {
		this.traveltype = traveltype;
	}

	public int getNumberSeats() {
		return numberSeats;
	}

	public void setNumberSeats(int numberSeats) {
		this.numberSeats = numberSeats;
	}

	public String getSeats() {
		return seats;
	}

	public void setSeats(String seats) {
		this.seats = seats;
	}
}
