package com.miw.alsa.model;

import org.hibernate.validator.constraints.NotEmpty;

public class Cities {
	
	@NotEmpty
	private int id;
	private String cityName;
	private int popularDestination;
	
	public Cities(){
		super();
	}

	public Cities(int id, String cityName, int popularDestination) {
		super();
		this.id = id;
		this.cityName = cityName;
		this.popularDestination = popularDestination;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public int getPopularDestination() {
		return popularDestination;
	}

	public void setPopularDestination(int popularDestination) {
		this.popularDestination = popularDestination;
	}
}
