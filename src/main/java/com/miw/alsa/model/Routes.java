package com.miw.alsa.model;

import org.hibernate.validator.constraints.NotEmpty;

public class Routes {
	
	@NotEmpty
	private int id;
	private String departure;
	private String arrival;
	private String destdeparture;
	private String destarrival;
	private double onewayPrice;
	private double returnPrice;
	private int cityDeparture;
	private int cityDestination;
	
	public Routes(){
		super();
	}

	public Routes(int id, String departure, String arrival,
			String destdeparture, String destarrival, double onewayPrice,
			double returnPrice, int cityDeparture, int cityDestination) {
		super();
		this.id = id;
		this.departure = departure;
		this.arrival = arrival;
		this.destdeparture = destdeparture;
		this.destarrival = destarrival;
		this.onewayPrice = onewayPrice;
		this.returnPrice = returnPrice;
		this.cityDeparture = cityDeparture;
		this.cityDestination = cityDestination;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDeparture() {
		return departure;
	}

	public void setDeparture(String departure) {
		this.departure = departure;
	}

	public String getArrival() {
		return arrival;
	}

	public void setArrival(String arrival) {
		this.arrival = arrival;
	}

	public String getDestdeparture() {
		return destdeparture;
	}

	public void setDestdeparture(String destdeparture) {
		this.destdeparture = destdeparture;
	}

	public String getDestarrival() {
		return destarrival;
	}

	public void setDestarrival(String destarrival) {
		this.destarrival = destarrival;
	}

	public double getOnewayPrice() {
		return onewayPrice;
	}

	public void setOnewayPrice(double onewayPrice) {
		this.onewayPrice = onewayPrice;
	}

	public double getReturnPrice() {
		return returnPrice;
	}

	public void setReturnPrice(double returnPrice) {
		this.returnPrice = returnPrice;
	}

	public int getCityDeparture() {
		return cityDeparture;
	}

	public void setCityDeparture(int cityDeparture) {
		this.cityDeparture = cityDeparture;
	}

	public int getCityDestination() {
		return cityDestination;
	}

	public void setCityDestination(int cityDestination) {
		this.cityDestination = cityDestination;
	}
}
