package com.miw.alsa.business;

import java.util.Vector;

import com.miw.alsa.model.Cities;


public interface CitiesManagerService {
	public int getCityByName(String cityName) throws Exception;
	public Vector<Cities> getCitiesName() throws Exception;
	public void addCounterPopularDestination(String cityName) throws Exception;
	
}
