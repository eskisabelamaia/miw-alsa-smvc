package com.miw.alsa.business;

import com.miw.alsa.model.UsersAccount;

public interface UsersAccountManagerService {
	public int getAccountByUsername(String username) throws Exception;
	public boolean createUsersAccount(UsersAccount account) throws Exception;
	public boolean verifyAccount(UsersAccount account) throws Exception;
}
