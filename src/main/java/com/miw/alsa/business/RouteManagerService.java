package com.miw.alsa.business;

import java.util.Vector;

import com.miw.alsa.model.Routes;


public interface RouteManagerService {
	public Routes getRouteById(Integer id) throws Exception;
	Vector<Routes> getRoutes(int departure, int arrival) throws Exception;
	int getRouteIdByCityId(int dedparture, int arrival) throws Exception;
}
