package com.miw.alsa.persistence;

import com.miw.alsa.model.UsersAccount;

public interface UsersAccountDataService {
	public int getAccountByUsername(String username) throws Exception;
	public boolean createUsersAccount(UsersAccount account) throws Exception;
	public boolean verifyAccount(UsersAccount account) throws Exception;
}
