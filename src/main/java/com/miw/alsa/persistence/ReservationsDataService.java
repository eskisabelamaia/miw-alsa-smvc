package com.miw.alsa.persistence;

import java.util.Vector;

import com.miw.alsa.model.Reservations;

public interface ReservationsDataService {
	public Vector<Reservations> getReservationsByUserId(int idUser) throws Exception;
	public Reservations getReservationByReservationCode(String reservationCode) throws Exception;
	boolean createReservation(String reservationcode, String departuredate,
			String returndate, String traveltype, int numberseats,
			String seats, String additionalopt, boolean hasinsurance,
			int iduser, int idroute, String citydeparture,
			String citydestination, double price) throws Exception;
	public boolean deleteReservationByReservationCode(String reservationCode) throws Exception;
	
}
