package com.miw.alsa.persistence;

public interface UsersDataService {
	public boolean createUser(String firstName, String middleName, String surname,
			String indetificationDocument, String identificationNumber, int idaccount) throws Exception;
	public int getUserByIdAccount(int idaccount) throws Exception;
}
