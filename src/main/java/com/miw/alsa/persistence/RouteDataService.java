package com.miw.alsa.persistence;

import java.util.Vector;

import com.miw.alsa.model.Routes;

public interface RouteDataService {
	public Routes getRouteById(Integer id) throws Exception;
	Vector<Routes> getRoutes(int departure, int arrival) throws Exception;
	int getRouteIdByCityId(int departure, int arrival) throws Exception;	
}
