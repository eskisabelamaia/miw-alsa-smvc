# Alsa

ALSA is an application to book bus tickets and it was created to initialize with Spring MVC and Hypersonic(hsqldb).

The application complies with the following functionalities:

    - Search bus schedules
    - Sort the schedules by time, price and day
    - Select seats
    - Introduce personal information
    - User authentication
    - Find reservation
    - Cancel reservation
    - Show the most popular destinations
    - Choose the language of the application between Spanish and English
